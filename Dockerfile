# FROM node:18-alpine AS development

# WORKDIR /app
# EXPOSE 3000

# COPY package.json yarn.lock* ./
# RUN yarn install --frozen-lockfile --check-files
# COPY . .
# CMD ["yarn", "dev"]

# FROM development AS production
# RUN yarn install --frozen-lockfile --check-files --production
# RUN yarn build

# CMD ["yarn", "start"]

# Development stage
FROM node:18-alpine AS development

WORKDIR /app
EXPOSE 3000

COPY package.json yarn.lock* ./
RUN yarn install --frozen-lockfile --check-files
COPY . .
CMD ["yarn", "dev","-L"]

# Production stage
FROM node:18-alpine AS production

WORKDIR /app
COPY package.json yarn.lock* ./
RUN yarn install --frozen-lockfile --check-files --production
COPY . .
RUN yarn build

CMD ["yarn", "start"]