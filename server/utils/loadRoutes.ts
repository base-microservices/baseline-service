import express from 'express'
import { readdirSync, statSync } from 'fs'
import { join, extname, basename } from 'path'

const loadRoutes = (
  dir: string,
  basePath: string = '/api',
  server: express.Express
) => {
  const files = readdirSync(dir)

  files.forEach((file) => {
    const fullPath = join(dir, file)
    const fileStat = statSync(fullPath)

    if (fileStat.isDirectory()) {
      loadRoutes(fullPath, `${basePath}/${basename(fullPath)}`, server)
    } else if (extname(file) === '.ts' || extname(file) === '.js') {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const route = require(fullPath).default
      let routePath = `${basePath}/${basename(file, extname(file))}`

      // Bỏ từ "index" ra khỏi đường dẫn
      if (basename(file, extname(file)) === 'index') {
        routePath = basePath
      }

      console.log('PATH: ', routePath)

      server.use(routePath, route)
    }
  })
}

export default loadRoutes
